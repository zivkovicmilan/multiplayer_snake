const Buttons = (function buttons() {
    'use strict';

    console.log("setting up buttons");

    const left = document.querySelector('button#left');
    const straight = document.querySelector('button#straight');
    const right = document.querySelector('button#right');
    const buttons_background = left.parentNode;

    let allow_reset_to_straight = true;

    Object.entries({
        'Left':left,
        'Straight':straight,
        'Right':right,
    }).forEach(([name, btn], _) => {
        console.log(name);
        btn.addEventListener('click', mk_msg_send(name));
    });

    focus_center();

    document.addEventListener('keydown', handle_keypress);

    function mk_msg_send(action) {
        return function() {
            //console.log("sending", action);
            this.focus();
            send(action);
            allow_reset_to_straight = true;
        }
    }

    function focus_center() {
        if (allow_reset_to_straight) {
            straight.focus();
            allow_reset_to_straight = false;
        }
    }

    function handle_keypress(ev) {
        //console.log(['handle_key', ev, ev.code, ev.keyCode]);
        switch (ev.keyCode) {
            case 37:
            case 65: // A
            case 74: // J
                left.click();
                break;
            case 38:
            case 87: // W
            case 73: // I
                straight.click();
                break;
            case 39:
            case 68: // D
            case 76: // L
                right.click();
                break;
        
            default:
                break;
        }
    }

    function send(action) {
        Main.send({ "PlayerInput": action });
    }

    return {
        focus_center: focus_center,
        update_controll_color: function (color) {
            buttons_background.style.backgroundColor = color;
        }
    }
})();
