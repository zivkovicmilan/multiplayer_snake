HOST="web_mps.estada.ch@yeti.estada.ch"
BINARY="multiplayer_snake"

deploy: build_release deploy_direct
	# done

deploy_direct:
	ssh ${HOST} -t "systemctl --user stop mps.service || true"
	rsync -avz --delete-after target/release/${BINARY} ./templates ./static ${HOST}:
	make systemd_restart

systemd_setup:
	ssh ${HOST} "mkdir -p ~/.config/systemd/user/"
	scp mps.service ${HOST}:~/.config/systemd/user/mps.service
	ssh ${HOST} -t "systemctl --user daemon-reload ; systemctl --user restart mps.service ; systemctl --user enable mps.service ; systemctl --user status mps.service"

systemd_stop:
	ssh ${HOST} -t "systemctl --user stop mps.service ; systemctl --user status mps.service"

systemd_restart:
	ssh ${HOST} -t "systemctl --user restart mps.service ; systemctl --user status mps.service"

build_release:
	cargo build --release

build_in_container_cached:
	docker run --rm -it --user "$$(id -u)":"$$(id -g)" -v "$$PWD":/usr/src/myapp -v "$$HOME"./cargo/registry/:/usr/local/cargo/registry/:ro -w /usr/src/myapp rust:latest cargo build --release
	chown "$$(id -u)":"$$(id -g)" target/release/${BINARY}

build_in_container:
	docker run --rm --user "$$(id -u)":"$$(id -g)" -v "$$PWD":/usr/src/myapp -w /usr/src/myapp rust:latest cargo build --release
	chown "$$(id -u)":"$$(id -g)" target/release/${BINARY}

ssh:
	ssh ${HOST}

ws_debug:
	websocat ws://localhost:8080/ws/